libdbicx-sugar-perl (0.0200-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Update lintian override info format in d/source/lintian-overrides on line
    2-7.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 13:55:36 +0000

libdbicx-sugar-perl (0.0200-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Add test dependency on libclone-perl. (Closes: #996258)
  * Update debian/upstream/metadata.
  * Refresh lintian overrides.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Oct 2021 18:45:18 +0200

libdbicx-sugar-perl (0.0200-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Update watch file: Rewrite usage comment.
  * Simplify rules.
    Stop build-depend on licensecheck cdbs.
  * Fix stop recommend libdbd-sqlite3-perl.
  * Enable autopkgtest.
  * Set Rules-Requires-Root: no.
  * Declare compliance with Debian Policy 4.3.0.
  * Relax to (build-)depend unversioned on libsql-translator-perl:
    Needed version satisfied even in oldstable.
  * Mark build-dependencies needed only for testsuite as such.
  * Stop build-depend on dh-buildinfo.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Strip superfluous copyright signs.
    + Extend coverage of packaging.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.
  * Wrap and sort control file.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Feb 2019 09:18:11 +0100

libdbicx-sugar-perl (0.0100-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump to file format 4.
    + Watch only MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
  * Drop CDBS get-orig-source target: Use gbp import-orig --uscan.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Update copyright info: Extend coverage of Debian packaging.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-Git field: Use https protocol.
  * Modernize CDBS use: Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 25 Dec 2016 19:30:57 +0100

libdbicx-sugar-perl (0.0001-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#794874.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Aug 2015 16:17:20 +0200
